using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlMaps : MonoBehaviour
{
    public List<GameObject> listMapas;
    public int level;
    public Vector3 initPosition= new Vector3();

    private void Start()
    {
        level = 0;
        gameObject.transform.GetChild(level).gameObject.gameObject.SetActive(listMapas[level]);
    }

    public void nextMapSpawn()
    {
        save();
        mapHide(level);
        level++;
        if (level < listMapas.Capacity)
        {
            GameObject.Find("Player").transform.position = initPosition;
            gameObject.transform.GetChild(level).gameObject.gameObject.SetActive(listMapas[level]);
        }
        else
        { 
            SceneManager.LoadScene("GameOverScene"); 
        }

    }
    public void mapHide(int level)
    {
            gameObject.transform.GetChild(level).gameObject.gameObject.SetActive(false);
    }

    public void save()
    {
        PlayerPrefs.SetInt("Coins", GameManager.coins);
        PlayerPrefs.SetInt("BulletsLevel", GameManager.Instance.BulletsLevel);
        PlayerPrefs.SetInt("MovementLevel", GameManager.Instance.MovementLevel);
        PlayerPrefs.SetInt("PointsLLifeLevelife", GameManager.Instance.LifeLevel);
        PlayerPrefs.SetInt("Health", GameManager.health);
        PlayerPrefs.SetInt("Score", GameManager.Instance.getScore());
        PlayerPrefs.Save();
    }
}
