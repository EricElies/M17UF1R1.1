using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class Shotting : MonoBehaviour
{
    public Transform firePoint;
    public GameObject player;
    
    public float bulletForce = 20f;
    public int numBullet;
    public int numBulletMax;
    //public ItemShopSC IncreaseBullet;
    
    public Weapons weapons;
    
    

    // Start is called before the first frame update
    void Start()
    {
        
        weapons.bulletType.numberBulletsMAX1 = weapons.bulletType.numberBulletsMAX;
        weapons.bulletType.numberBullets = weapons.bulletType.numberBulletsMAX1;
        //gameObject.GetComponent<Bullets>().numberBullets;

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E)) 
        {
            weapons = gameObject.GetComponent<InventoryWeapons>().changeWeapon(+1);

            gameObject.GetComponentInChildren<SpriteRenderer>().sprite = weapons.sprite;


        }
       
        if (Input.GetKeyDown(KeyCode.Q))
        {
            weapons = gameObject.GetComponent<InventoryWeapons>().changeWeapon(-1);

            gameObject.GetComponentInChildren<SpriteRenderer>().sprite = weapons.sprite;

        }
        /*if (EventSystem.current.IsPointerOverGameObject())
            return ;
        */
        if (player.GetComponent<Shotting>().weapons.bulletType.numberBullets > 0)
        {
            if (Input.GetButtonDown("Fire1") )
            {
                weapons.Shoot(firePoint);
                weapons.bulletType.numberBullets--;
                
            }
        }

        weapons.bulletType.numberBulletsMAX1 = weapons.bulletType.numberBulletsMAX + GameManager.Instance.BulletsLevel;

    }
    
   
}
