using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public static int scoreValue = 0;
    public Text  score, ammo, goldText;
    public Image weaponImage;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        score.text = "Score: " + GameManager.Instance.getScore().ToString();
        ammo.text = "" + GameObject.Find("Player").GetComponent<Shotting>().weapons.bulletType.numberBullets;
        goldText.text = "Gold: " + GameManager.Instance.getCoins().ToString();
        weaponImage.sprite = GameObject.Find("Player").GetComponent<Shotting>().weapons.sprite;
    }
}
