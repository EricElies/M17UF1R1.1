using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DataUI : MonoBehaviour
{
        public Text text;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        text.text = "Coins: " + PlayerPrefs.GetInt("Coins", GameManager.coins) + " \n \nBulletLevel: " +
                PlayerPrefs.GetInt("BulletsLevel", GameManager.Instance.BulletsLevel) + " \n \nMovementLevel: " +
                 PlayerPrefs.GetInt("MovementLevel", GameManager.Instance.MovementLevel) + " \n \nLifeLevel: " +
                 PlayerPrefs.GetInt("PointsLLifeLevelife", GameManager.Instance.LifeLevel) + " \n \nHealth: " +
                 PlayerPrefs.GetInt("Health", GameManager.Instance.getHealth()) + " \n \n Score: " +
                 PlayerPrefs.GetInt("Score", GameManager.Instance.getScore());
    }
}
