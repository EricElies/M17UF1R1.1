using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Item")]
public class ItemScriptableObject : ScriptableObject
{
    public string nameItem;
    public string chance;
    public int price;
    public string typeItem;
    public Sprite sprite;
}

