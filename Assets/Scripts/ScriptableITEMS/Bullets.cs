using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Bullet", menuName = "Bullet")]
public class Bullets : ScriptableObject
{
    public GameObject bulletPrefab;
    public string nameBullet;
    public Sprite sprite;
    public int damageBullet;
    public int numberBullets;
    public int numberBulletsMAX;
    public int numberBulletsMAX1;
}
