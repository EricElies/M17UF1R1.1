using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New Weapon", menuName ="Weapon")]
public class Weapons : ScriptableObject
{
    public string nameWeapon;
    public Sprite sprite;
    public float forceWeapon;
    public Bullets bulletType;



    internal void Shoot(Transform firePoint)
    {
        if (nameWeapon == "Shotgun")
        {
            for (int i = 0; i < 3; i++)
            {
                
                switch (i) 
                { 
                    case 0:
                        GameObject bullet1 = Instantiate(bulletType.bulletPrefab, firePoint.position, firePoint.rotation * Quaternion.Euler(1f, 1f, 0));
                        Rigidbody2D rb1 = bullet1.GetComponent<Rigidbody2D>();
                        rb1.AddForce(firePoint.up * forceWeapon , ForceMode2D.Impulse);
                        
                        break;
                    case 1:
                        GameObject bullet = Instantiate(bulletType.bulletPrefab, firePoint.position, firePoint.rotation);
                        Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
                        rb.AddForce(firePoint.up * forceWeapon, ForceMode2D.Impulse);

                        break;
                    case 2:
                        GameObject bullet2 = Instantiate(bulletType.bulletPrefab, firePoint.position , firePoint.rotation*Quaternion.Euler(1f,1f,0) );
                        Rigidbody2D rb2 = bullet2.GetComponent<Rigidbody2D>();
                        
                        rb2.AddForce(firePoint.up * forceWeapon, ForceMode2D.Impulse);

                        break;
                    default: 
                        break;

                }
                


            }
        }

        else
        {
            GameObject bullet = Instantiate(bulletType.bulletPrefab, firePoint.position, firePoint.rotation);
            Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
            rb.AddForce(firePoint.up * forceWeapon, ForceMode2D.Impulse);
        }
    }

    
}
