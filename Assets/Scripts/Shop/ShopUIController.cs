
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class ItemShop
{
    public ItemShopSC InfoItem;
    public Text TextNameItem;
    public Text TextDescriptionItem;
    public Text TextCostItem;
    public Button ButtonBuyItem;
}

public class ShopUIController : MonoBehaviour
{
    public ItemShop[] Items;
    public int level;





    // Start is called before the first frame update
    void Start()
    {
        if (level <= 3)
        {
            setItem(Items[0], GameManager.Instance.LifeLevel);
            setItem(Items[1], GameManager.Instance.BulletsLevel);
            setItem(Items[2], GameManager.Instance.MovementLevel);
        }
        else {
            Debug.Log("Can't BUY!");
        }
        
    }

    
    
    private void setItem(ItemShop item, int level)
    {

        if (level == 3)
        {
            item.ButtonBuyItem.GetComponent<Button>().enabled = false;
            item.TextNameItem.text = item.InfoItem.Name;
            item.TextDescriptionItem.text = item.InfoItem.Description;
            item.TextCostItem.text = item.InfoItem.Cost[3].ToString();
            
        }
        else if (level == 2)
        {
            item.TextNameItem.text = item.InfoItem.Name;
            item.TextDescriptionItem.text = item.InfoItem.Description ;
            item.TextCostItem.text = item.InfoItem.Cost[2].ToString() ;
            
        }
        else if (level == 1)
        {
            item.TextNameItem.text = item.InfoItem.Name;
            item.TextDescriptionItem.text = item.InfoItem.Description ;
            item.TextCostItem.text = item.InfoItem.Cost[1].ToString();
            
        }
        else
        {
            item.TextNameItem.text = item.InfoItem.Name;
            item.TextDescriptionItem.text = item.InfoItem.Description ;
            item.TextCostItem.text = item.InfoItem.Cost[0].ToString();
            
        }
    }

    // Update is called once per frame
    void Update()
    {
       
    }
    
    

    public void buy(int position)
    {
       
       
        int level;
        if (position == 0)
        {
            level = GameManager.Instance.LifeLevel;
        }
        else if (position == 1)
        {
            level = GameManager.Instance.BulletsLevel;
        }
        else 
        {
            level = GameManager.Instance.MovementLevel;
        }
        
        if (level >= 2)
        {
            Items[position].ButtonBuyItem.GetComponent<Button>().enabled = false;
            Items[position].TextCostItem.color = Color.black;
            Debug.Log("Can't BUY!");
        }

        if (GameManager.Instance.getCoins() >= Items[position].InfoItem.Cost[level])
        {

            GameManager.Instance.setCoins(GameManager.Instance.getCoins() - Items[position].InfoItem.Cost[level]);

            if (position == 0)
            {
                GameManager.Instance.LifeLevel++;
                GameManager.Instance.setMaxHealth(GameManager.Instance.getMaxHealth() + Items[position].InfoItem.Count[level]);
                GameManager.Instance.setHealth(GameManager.Instance.getMaxHealth());

            }
            else if (position == 1)
            {
                GameManager.Instance.BulletsLevel++;
                /*GameObject.Find("Player").GetComponent<Shotting>().weapons.bulletType.numberBulletsMAX1 += Items[position].InfoItem.Count[level];
                Debug.Log(GameObject.Find("Player").GetComponent<Shotting>().weapons.bulletType.numberBulletsMAX1);*/
            }
            else
            {
                GameManager.Instance.MovementLevel++;
                /*PlayerMovement.moveSpeed  += Items[position].InfoItem.Count[level];
                Debug.Log(PlayerMovement.moveSpeed);*/

            }
            level++;
        }
        else { Debug.Log("Can't BUY!"); }
            
        
        if (level >= 3)
            {
                Items[position].ButtonBuyItem.GetComponent<Button>().enabled = false;
                Items[position].TextCostItem.color = Color.black;
                Debug.Log("Can't BUY!");
            }
            else {setItem(Items[position], level); }
                
            


            
           
        }
        
        
    }

    


