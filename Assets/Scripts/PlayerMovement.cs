using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public static int moveSpeed = 5;
    public Rigidbody2D rb;
    
    public Camera cam;
    Vector2 movement;
    Vector2 mousePos;
    public float DashColdown;
    public GameObject DashParticle;
    public float DashForce = 50f;
    public ItemShopSC IncreaseMovement;




    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        DashColdown = 0;
        cam.transform.position = rb.transform.position;
        

    }

    // Update is called once per frame
    void Update()
    {
        

    }

    private void FixedUpdate()
    {
        movement = GetBaseInput();
        mousePos = cam.ScreenToWorldPoint(Input.mousePosition);

        DashColdown -= Time.deltaTime;
        rb.MovePosition(rb.position + movement * (moveSpeed + GameManager.Instance.MovementLevel) * Time.deltaTime);
        Vector2 lookDir = mousePos - rb.position;
        float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg - 90f;
        rb.rotation = angle;
    }

    private Vector3 GetBaseInput()
    { //returns the basic values, if it's 0 than it's not active.
        Vector3 p_Velocity = new Vector3();
        if (Input.GetKey(KeyCode.W))
        {
            p_Velocity += new Vector3(0, 1, 0);          
        }
        if (Input.GetKey(KeyCode.S))
        {
            p_Velocity += new Vector3(0, -1, 0);             
        }       
        if (Input.GetKey(KeyCode.A))
        {
            p_Velocity += new Vector3(-1, 0, 0);            
        }      
        if (Input.GetKey(KeyCode.D))
        {
            p_Velocity += new Vector3(1, 0, 0);             
        }
        if (Input.GetKey(KeyCode.Space) && DashColdown <= 0)
        {
            p_Velocity = Dash(p_Velocity);
        }
        return p_Velocity;
    }


    public Vector3 Dash( Vector3 p_Velocity) {
        GameObject dashObject;
        dashObject = Instantiate(DashParticle, transform.position, transform.rotation);
        DashColdown = 3f;
        Destroy(dashObject, 1);
        return p_Velocity *= DashForce;
        
    }

    
}

