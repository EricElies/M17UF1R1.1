using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class ChangeScene : MonoBehaviour
{
    public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public static void delete()
    {
        PlayerPrefs.DeleteKey("Coins");
        PlayerPrefs.DeleteKey("BulletsLevel");
        PlayerPrefs.DeleteKey("MovementLevel");
        PlayerPrefs.DeleteKey("PointsLLifeLevelife");
        PlayerPrefs.DeleteKey("Health");
        PlayerPrefs.DeleteKey("Score");

        PlayerPrefs.Save();
        Application.Quit();
    }
}

