using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Munition : MonoBehaviour
{
    public  GameObject DashParticle;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == ("Player")) 
        {
            List<Weapons> list = collision.gameObject.GetComponent<InventoryWeapons>().listWeapons;
            foreach (Weapons item in list)
            {
                item.bulletType.numberBullets = item.bulletType.numberBulletsMAX1; 
            }
            GameObject dashObject;
            dashObject = Instantiate(DashParticle, transform.position, transform.rotation);
            
            Destroy(dashObject, 1);
            Destroy(this.gameObject);
        }
    }
}
