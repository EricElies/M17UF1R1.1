using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretEnemy : MonoBehaviour
{
    
    [SerializeField] private GameObject bulletEnemy;
    [SerializeField] private Transform firepoint;
    [SerializeField] private float fireDelay = 1f;
    
    public int healthEnemy;
    public GameObject DieParticle, gold;
    private Transform player;
    


    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player").transform;
        GameManager.Instance.numEnemy++;
        InvokeRepeating("Shoot", 0f, fireDelay);
    }

    // Update is called once per frame
    void Update()
    {

        
        LookAtTarget();
       
    }
    
    

    private void LookAtTarget() 
    {
        if (player != null) 
        {
            /*Vector3 direction = player.transform.position - transform.position;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 180f;
            transform.rotation =  Quaternion.AngleAxis(angle, Vector3.forward);*/
            Vector3 direction = player.transform.position - transform.position;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg ;
            transform.rotation = Quaternion.AngleAxis(angle - 180f, Vector3.forward);
        }
    }

    void Shoot()
    {   
        if (player != null){
            GameObject bullet = Instantiate(bulletEnemy, firepoint.position, firepoint.rotation);
            Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
            rb.AddForce(player.position - transform.position, ForceMode2D.Impulse); 
        }
    }

    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            healthEnemy--;

            if (healthEnemy <= 0)
            {
                GameManager.Instance.setScore(GameManager.Instance.getScore() + 100);
                gold = Instantiate(gold, transform.position, transform.rotation);
                
                GameObject dieObject;
                dieObject = Instantiate(DieParticle, transform.position, transform.rotation);
                Destroy(dieObject, 1);
                Destroy(this.gameObject);
                GameManager.Instance.numEnemy--;


            }
        }

        if (collision.gameObject.CompareTag("Rocket"))
        {
            healthEnemy-=5;

            if (healthEnemy <= 0)
            {
                GameManager.Instance.setScore(GameManager.Instance.getScore() + 100); 
                gold = Instantiate(gold, transform.position, transform.rotation); 

                GameObject dieObject;
                dieObject = Instantiate(DieParticle, transform.position, transform.rotation);
                Destroy(dieObject, 1);
                Destroy(this.gameObject);
                GameManager.Instance.numEnemy--;


            }
        }
    }


}
