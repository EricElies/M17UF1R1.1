using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class EnemyManager : MonoBehaviour
{
    public int healthEnemy;
    public Slider healthBarPlayer;
    public GameObject DieParticle, gold;
    public PlayerMovement PlayerMovement;
    public GameObject player;
    public float speed = 1.0f;
    public CircleCollider2D rangExplosio;
    public bool playerOnRange, Exploiting;


    



    // Start is called before the first frame update

    void Start()
    {
        Exploiting = false;
        playerOnRange = false;
        player = GameObject.Find("Player");
        rangExplosio = GetComponent<CircleCollider2D>();
        GameManager.Instance.numEnemy++;

    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    void Move() {
        float step = speed * Time.deltaTime;
        if (!playerOnRange)
        {
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position, step);
        }

        if (transform.position.x < GameObject.Find("Player").transform.position.x)
        {
            GetComponent<SpriteRenderer>().flipX = true;
        }
        else
        {
            GetComponent<SpriteRenderer>().flipX = false;
        }

        
    }

    
    public  void dieEnemy() {
        GameManager.Instance.setScore(GameManager.Instance.getScore() + 10);
        GameObject dieObject;
        DieParticle.transform.localScale = new Vector3(3, 3, 0);
        dieObject = Instantiate(DieParticle, transform.position, transform.rotation);
        player.GetComponent<HealthBar>().takeDamageExplosion(5);
        Destroy(dieObject, 1);
        Destroy(this.gameObject);
        GameManager.Instance.numEnemy--;
    }

    

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Bullet") && !Exploiting)
        {
            healthEnemy--;
            
            if (healthEnemy <= 0)
            {
                GameManager.Instance.setScore(GameManager.Instance.getScore() + 10);
                gold = Instantiate(gold, transform.position, transform.rotation);
                GameObject dieObject;
                dieObject = Instantiate(DieParticle, transform.position, transform.rotation);
                Destroy(dieObject, 1);               
                Destroy(this.gameObject);
                GameManager.Instance.numEnemy--;


            }
        }

        if (collision.gameObject.CompareTag("Rocket")&& !Exploiting)
        {
            healthEnemy -= 5;

            if (healthEnemy <= 0)
            {
                GameManager.Instance.setScore(GameManager.Instance.getScore() + 10);
                gold = Instantiate(gold, transform.position, transform.rotation);
                GameObject dieObject;
                dieObject = Instantiate(DieParticle, transform.position, transform.rotation);
                Destroy(dieObject, 1);
                Destroy(this.gameObject);
                GameManager.Instance.numEnemy--;


            }
        }
    }
}
