using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
    public GameObject enemy;

    void Start()
    {
        InvokeRepeating("Spawner", 1, 5);
    }

    // Update is called once per frame
    void Spawner()
    {
        GameObject e = Instantiate(enemy, new Vector3(Random.Range(3f, 5f), Random.Range(-4f, 4f)), Quaternion.identity);

    }
}
