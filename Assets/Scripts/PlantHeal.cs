using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantHeal : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        HealthBar targetHealth = collision.GetComponent<HealthBar>();
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(gameObject);
            // heals 3 points of life
            targetHealth.takeHeal(3);
        }
    }
}