using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class exitCollider : MonoBehaviour
{
    
    private void Start()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (GameManager.Instance.numEnemy <= 0)
        {
            if (collision.gameObject.CompareTag("Player"))
            {    
               GameObject.Find("ControlMap").gameObject.GetComponent<ControlMaps>().nextMapSpawn();
            }
        }
        else
        {
            Debug.Log("Kill All Enemies to Exit!");
        }
    }
    
    
    public void Save()
    {
        PlayerPrefs.GetInt("Coins", GameManager.Instance.getCoins());
        PlayerPrefs.GetInt("BulletsLevel", GameManager.Instance.BulletsLevel);
        PlayerPrefs.GetInt("MovementLevel", GameManager.Instance.MovementLevel);
        PlayerPrefs.GetInt("PointsLLifeLevelife", GameManager.Instance.LifeLevel);

        
        PlayerPrefs.Save();
    }

}
