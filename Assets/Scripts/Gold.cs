using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gold : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.CompareTag("Player"))
        {
            if (gameObject.CompareTag("Gold"))
            {
                // increase Gold

                GameManager.Instance.setCoins(GameManager.Instance.getCoins() + 100);
                Destroy(gameObject);
            }
            else if (gameObject.CompareTag("Gold500"))
            {
                // increase Gold
                GameManager.Instance.setCoins(GameManager.Instance.getCoins() + 500);
                Destroy(gameObject);
            }
        }
    }
}
