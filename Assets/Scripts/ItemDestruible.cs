using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDestruible : MonoBehaviour
{
    
    [SerializeField] private GameObject gold, gold500, Munition, plantHeal;
    [SerializeField] private Sprite box;

    private Vector3 posicionItem;
    private bool chestOpened = false;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if ((collision.gameObject.tag == "Bullet" || collision.gameObject.tag == "Rocket") && !chestOpened )
        {
            chestOpened = true;
            int val = Random.Range(1, 6);
            
                
            if (transform.position.y < 0) { posicionItem = new Vector3(0, -1, 0); }
            else { posicionItem = new Vector3(0, 1, 0); }

            if (val <= 2)
            {
                Instantiate(plantHeal, transform.position - posicionItem, transform.rotation);

            }
            else if (val >= 3 && val < 4)
            {
                Instantiate(gold, transform.position - posicionItem, transform.rotation);
            }
            else if (val >= 4 && val < 5)
            {
                Instantiate(Munition, transform.position - posicionItem, transform.rotation);
            }
            else if (val >= 5)
            {
                Instantiate(gold500, transform.position - posicionItem, transform.rotation);
            }
            gameObject.GetComponent<SpriteRenderer>().sprite = box;

            
            
        }
    }
}
