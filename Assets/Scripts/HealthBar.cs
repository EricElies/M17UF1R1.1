using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public int vidas;
    public Slider Slider;
    public GameObject Fill;
    private bool rangeExplosion;
    public GameObject UIShop;
    public static bool isOpenShop = false;

    private void Start()
    {
        vidas = GameManager.Instance.getMaxHealth(); 
        GameManager.Instance.setHealth(vidas);
        rangeExplosion = false;
        Slider.maxValue = vidas;
        Slider.value = Slider.maxValue;
    }

    void Update()
    {
        Slider.maxValue = GameManager.Instance.getMaxHealth();
        vidas = GameManager.Instance.getHealth();
        controlSliderColor();
        Slider.value = vidas;
    }

    private void controlSliderColor()
    {
        if (Slider.value <= 7) 
        {
            Fill.GetComponent<Image>().color=Color.yellow;
        }
        if (Slider.value <= 4)
        {
            Fill.GetComponent<Image>().color = Color.red;
        }
        if (Slider.value > 7)
        {
            Fill.GetComponent<Image>().color = Color.cyan;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy") || collision.gameObject.CompareTag("BulletEnemy"))
        {
            takeDamage(1);
            StartCoroutine(takingDamage(gameObject)); 
        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().color = Color.white;
        }
    }
    
    public IEnumerator coroutineA(GameObject collision)
    {
        collision.gameObject.GetComponent<EnemyManager>().playerOnRange = true;
        collision.gameObject.GetComponent<EnemyManager>().Exploiting = true;
        collision.gameObject.GetComponent<SpriteRenderer>().color = Color.red;
        yield return new WaitForSeconds(2.0f);
        collision.gameObject.GetComponent<EnemyManager>().dieEnemy();
        collision.gameObject.GetComponent<EnemyManager>().playerOnRange = false;
        collision.gameObject.GetComponent<EnemyManager>().Exploiting = false;
    }

    public IEnumerator takingDamage(GameObject collision)
    {

        gameObject.transform.GetChild(1).gameObject.GetComponent<SpriteRenderer>().color = Color.red;
        yield return new WaitForSeconds(0.3f);
        gameObject.transform.GetChild(1).gameObject.GetComponent<SpriteRenderer>().color = Color.white;

    }
    public IEnumerator takingHeal(GameObject collision)
    {

        gameObject.transform.GetChild(1).gameObject.GetComponent<SpriteRenderer>().color = Color.cyan;
        yield return new WaitForSeconds(0.3f);
        gameObject.transform.GetChild(1).gameObject.GetComponent<SpriteRenderer>().color = Color.white;

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("MiniEnemy"))
        {
            rangeExplosion = false;
        }
        if (collision.gameObject.CompareTag("Shop"))
        {
            UIShop.SetActive(false);
            //Set to 0 for pause the game
            //Time.timeScale = 1f;
        }
    }
    

    public void OnTriggerEnter2D(Collider2D collision)
    { 
        if (collision.gameObject.CompareTag("MiniEnemy") )
        {
            rangeExplosion = true;

            if (!collision.gameObject.GetComponent<EnemyManager>().Exploiting) { 
                StartCoroutine(coroutineA(collision.gameObject));   
            }

        }
        if (collision.gameObject.CompareTag("Shop")) 
        {
            UIShop.SetActive(true);
            
        }
    }
    
   

    public void takeDamageExplosion(int v)
    {
        if (rangeExplosion) {
            takeDamage(5);   
            StartCoroutine(takingDamage(gameObject));
            rangeExplosion = false;
        }
    }

    public void takeDamage(int v)
    {
        GameManager.Instance.getDamage(v);
        StartCoroutine(takingDamage(gameObject));
    }

    public void takeHeal(int v)
    {
        GameManager.Instance.getHeal(v);
        StartCoroutine(takingHeal(gameObject));
    }

}
