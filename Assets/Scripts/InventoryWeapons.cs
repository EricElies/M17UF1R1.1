using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class InventoryWeapons : MonoBehaviour
{
    public List<Weapons> listWeapons;
    private int currentWeapon;
    
    // Start is called before the first frame update
    void Start()
    {
        currentWeapon = 0;
        

    }

    public Weapons changeWeapon(int x)
    {
        currentWeapon += x;
        if (currentWeapon > listWeapons.ToArray().Length - 1)
        {
            currentWeapon = 0;
        }
        else if (currentWeapon < 0)
        {
            currentWeapon = listWeapons.ToArray().Length - 1;
        }
        return listWeapons[currentWeapon];
    }

    // A�adir armas a la lista
    public void addWeapon(Weapons w) {
        listWeapons.Add(w);
    }

    // Update is called once per frame
    void Update()
    {
   


}
}
