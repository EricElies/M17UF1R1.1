using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;

    // Coins
    public static int coins;
    public void setCoins(int actualCoins) {
        coins = actualCoins;
    }
    public int getCoins()
    {
        return coins;
    }


    // Health
    public static int health;
    public void setHealth(int actualHealth)
    {
        health = actualHealth;
    }
    public int getHealth()
    {
        return health;
    }

    // MaxHealth
    private static int maxHealth = 10;
    public void setMaxHealth(int actualHealth)
    {
        maxHealth = actualHealth;
    }

    

    public int getMaxHealth()
    {
        return maxHealth;
    }

    //Score
    private static int score; 
    public void setScore(int actualScore)
    {
        score = actualScore;
    }
    
    public int getScore()
    {
        return score;
    }
        

    

    public static GameManager Instance
    {
        get
        {
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance == null)
        {
            DontDestroyOnLoad(gameObject);
            _instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

   
    
    public int numEnemy;
    public static int levelDungeon = 0;
    
    public int LifeLevel = 0,
       BulletsLevel = 0,
       MovementLevel = 0;
        
    
    


    void Start()
    {
        //SaveData.delete();
        AudioListener.volume = PlayerPrefs.GetFloat("Volume", AudioListener.volume);
        
        coins = PlayerPrefs.GetInt("Coins", coins);
        BulletsLevel = PlayerPrefs.GetInt("BulletsLevel", BulletsLevel);
        MovementLevel = PlayerPrefs.GetInt("MovementLevel", MovementLevel);
        LifeLevel = PlayerPrefs.GetInt("PointsLLifeLevelife", LifeLevel);
        health = PlayerPrefs.GetInt("Health", health);
        score = PlayerPrefs.GetInt("Score", score);
        /*
        // Start Map
        Instantiate(listMapas[0], new Vector3(0, 0, 0), Quaternion.identity);

        //Start Player
        Instantiate(player, new Vector3(0, 0, 0), Quaternion.identity);
        */

    }

    // Update is called once per frame
    void Update()
    {
        //dead();
    }

    


    public void getDamage(int dmg) {
        health -= dmg;
        if (health <= 0)
            dead();
    }

    public void getHeal(int heal)
    {
        if ((health + heal) > maxHealth) 
        {
            health = maxHealth;
        }
        else health += heal;
    }

    public void dead()
    {
        if (health <= 0)
        {
            SceneManager.LoadScene("GameOverScene");
        }

    }

}
